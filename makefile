SHELL:=/bin/bash

DOMAIN:=example.com
include .env
export $(shell sed 's/=.*//' .env)

cluster-delete:
	k3d cluster delete argocd
	#docker volume rm argocd-volumes

cluster-create:
	k3d cluster create argocd \
		--servers 3 \
		-p $${HTTP_PORT:-80}:80@loadbalancer \
		-p $${HTTPS_PORT:-443}:443@loadbalancer \
		$${DOCKER_HOST:+--k3s-arg "--tls-san=$${DOCKER_HOST#*//}@server:*"} \
		--k3s-arg '--disable=traefik@server:*' \
		-v "argocd-volumes:/volumes@server:*"

deploy-ingress:
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud/deploy.yaml
	kubectl wait --for=condition=ready pod -l app.kubernetes.io/component=controller -n ingress-nginx --timeout=120s

cluster-recreate: cluster-delete cluster-create

deploy-argocd:
	kubectl describe ns/argocd 2>/dev/null || kubectl create namespace argocd
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
	@printf '%s\n' \
		'apiVersion: networking.k8s.io/v1' \
		'kind: Ingress' \
		'metadata:' \
		'  name: argocd-server' \
		'  annotations:' \
    	'    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"' \
    	'    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"' \
		'spec:' \
		'  ingressClassName: nginx' \
		'  rules:' \
		'  - host: argocd.$(DOMAIN)' \
		'    http:' \
		'      paths:' \
		'      - path: /' \
		'        pathType: Prefix' \
		'        backend:' \
		'          service:' \
		'            name: argocd-server' \
		'            port:' \
		'              name: https' \
		'' | tee /dev/stderr | kubectl apply -f - -n argocd
	kubectl wait --for=condition=ready pod -l app.kubernetes.io/name=argocd-server -n argocd --timeout=60s

cluster-autopilot-init:
	argocd-autopilot repo bootstrap

cluster-autopilot-recover:
	argocd-autopilot repo bootstrap --recover

cluster-recover:
# https://gitlab.com/-/snippets/2543586
	curl -s https://gitlab.com/-/snippets/2543586/raw/main/argocd-init.sh | bash

argocd-login:
	$(eval password := $(shell kubectl get secret/argocd-initial-admin-secret -n argocd -o template --template='{{.data.password}}' | base64 -d))
	argocd login argocd.$(DOMAIN)$${HTTPS_PORT:+:$$HTTPS_PORT} --insecure --username admin --password "$(password)" --grpc-web
	@echo -e "\n\x1b[0;2m***\x1b[0m You can longin into \x1b[34;4mhttps://argocd.$(DOMAIN)$${HTTPS_PORT:+:$$HTTPS_PORT}/\x1b[0m by admin's password \x1b[33m$(password)\x1b[0m\n"

argocd-watch:
	@watch kubectl get ing,application,pod -n argocd

hard-refresh-test-avp-app:
	argocd app get argocd/test-avp --hard-refresh
check-test-avp-secret:
	@kubectl get secret/test-secret -n default -o json | jq -r '.data|to_entries[]|.key+": "+.value' | awk '{("echo "$$2" | base64 -d")|getline $$2; print}'


.PHONY: .vault-verify-address
.vault-verify-address:
	@VAULT_ADDR="$$(awk '/^VAULT_ADDR=/{print substr($$0,12)}' .env)"; \
	VAULT_URL="http://vault.$(DOMAIN)$${HTTP_PORT:+:$$HTTP_PORT}"; \
	[ "$$VAULT_ADDR" == "$$VAULT_URL" ] || sed -i~ "s#^\(VAULT_ADDR=\).*\$$#\1$$VAULT_URL#" .env

.PHONY: .vault-verify-token
.vault-verify-token: .vault-verify-address
	$(eval export VAULT_ADDR = $(shell awk '/^VAULT_ADDR=/{print substr($$0,12)}' .env))
	set -o pipefail; vault status | awk '$$1=="Initialized"{r=$$2=="true"}END{exit !r}' || if [ $$? -eq 1 ]; then vault operator init | tee .secrets; fi
	set -o pipefail; vault status | awk '$$1=="Sealed"{r=$$2=="false"}END{exit !r}' || awk '/^Unseal Key [1-3]: /{system("set -x; vault operator unseal "$$NF)}' .secrets

	@VAULT_TOKEN="$$(awk '/^VAULT_TOKEN=/{print substr($$0,13)}' .env)"; \
	VAULT_ROOT_TOKEN="$$(awk '/^Initial Root Token: /{print $$NF}' .secrets)"; \
	[ "$$VAULT_TOKEN" == "$$VAULT_ROOT_TOKEN" ] || sed -i~ "s#^\(VAULT_TOKEN=\).*\$$#\1$$VAULT_ROOT_TOKEN#" .env	

vault-init: .vault-verify-token
	$(eval export VAULT_TOKEN = $(shell awk '/^VAULT_TOKEN=/{print substr($$0,13)}' .env))
	terraform -chdir=terraform/vault init
	terraform -chdir=terraform/vault plan
	terraform -chdir=terraform/vault apply -auto-approve
	@echo -e "\n\x1b[0;2m***\x1b[0m You can longin into \x1b[34;4mhttp://vault.$(DOMAIN)$${HTTP_PORT:+:$$HTTP_PORT}/\x1b[0m by token \x1b[33m$(VAULT_TOKEN)\x1b[0m\n"

