terraform {
  required_providers {
    vault = {
      source = "hashicorp/vault"
      version = "3.10.0"
    }
  }
}

provider "vault" {
}

resource "vault_mount" "apps" {
  path        = "apps"
  type        = "kv-v2"
  description = "Storage for applications secreats"
}

resource "vault_kv_secret_v2" "test_secret" {
  mount = vault_mount.apps.path
  name = "database"
  data_json = jsonencode(
  {
    username = "db_user",
    password = "db_secret"
  }
  )
}

resource "vault_policy" "argocd-policy" {
  name = "argocd"
  policy = <<EOT
path "apps/data/*"
{
  capabilities = ["list", "read"]
}
EOT
}

resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"
  description = "Authorization from kubernetes"
}
resource "vault_kubernetes_auth_backend_config" "example" {
  backend                = vault_auth_backend.kubernetes.path
  kubernetes_host        = "https://kubernetes.default.svc:443"
}
resource "vault_kubernetes_auth_backend_role" "argocd-backend-role" {
  backend                          = vault_auth_backend.kubernetes.path
  role_name                        = "argocd"
  bound_service_account_names      = ["argocd-repo-server"]
  bound_service_account_namespaces = ["argocd"]
  token_ttl                        = 3600
  token_policies                   = [vault_policy.argocd-policy.name]
}
resource "vault_identity_entity" "argocd-entity" {
  name      = "argocd"
  policies  = ["argocd"]
}
resource "vault_identity_entity_alias" "argocd-entity-alias" {
  name            = "argocd"
  mount_accessor  = vault_auth_backend.kubernetes.accessor
  canonical_id    = vault_identity_entity.argocd-entity.id
}

